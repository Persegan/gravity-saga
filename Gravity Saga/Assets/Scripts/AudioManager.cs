﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour {

    public static AudioManager Audio_Manager;

    public AudioClip Adventure_Begins;
    public AudioClip Song_2;
    public AudioClip Song_3;

    void Awake()
    {
        if (Audio_Manager == null)
        {
            DontDestroyOnLoad(gameObject);
            Audio_Manager = this;
            SceneManager.sceneLoaded += OnSceneWasLoaded;
        }
        else if (Audio_Manager != this)
        {
            Destroy(gameObject);
        }
    }

    void OnSceneWasLoaded(Scene scene, LoadSceneMode mode)
    {

        switch (scene.name)
        {
            case ("Vertical_Slice"):
            case ("Level_1"):
            case ("Level_2"):
            case ("Level_2.5"):
                if (GetComponent<AudioSource>().clip == Adventure_Begins && GetComponent<AudioSource>().isPlaying && GetComponent<AudioSource>().volume == 1.0f)
                {

                }
                else
                {
                    GetComponent<AudioSource>().volume = 0.0f;
                    GetComponent<AudioSource>().clip = Adventure_Begins;
                    GetComponent<AudioSource>().Play();
                    StartCoroutine(FadeIn(GetComponent<AudioSource>(), 5.0f));
                }
                break;

            case ("Level_3"):
            case ("Level_4"):
                if (GetComponent<AudioSource>().clip == Song_2 && GetComponent<AudioSource>().isPlaying && GetComponent<AudioSource>().volume == 1.0f)
                {

                }
                else
                {
                    GetComponent<AudioSource>().volume = 0.0f;
                    GetComponent<AudioSource>().clip = Song_2;
                    GetComponent<AudioSource>().Play();
                    StartCoroutine(FadeIn(GetComponent<AudioSource>(), 5.0f));
                }
                break;


            case ("Level_5"):
            case ("Level_6"):
                if (GetComponent<AudioSource>().clip == Song_3 && GetComponent<AudioSource>().isPlaying && GetComponent<AudioSource>().volume == 1.0f)
                {

                }
                else
                {
                    GetComponent<AudioSource>().volume = 0.0f;
                    GetComponent<AudioSource>().clip = Song_3;
                    GetComponent<AudioSource>().Play();
                    StartCoroutine(FadeIn(GetComponent<AudioSource>(), 5.0f));
                }
                break;

            default:

                break;
        }
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
    }


    public static IEnumerator FadeOut(AudioSource audioSource, float FadeTime)
    {
        float startVolume = audioSource.volume;

        while (audioSource.volume > 0)
        {
            audioSource.volume -= startVolume * Time.deltaTime / FadeTime;

            yield return null;
        }

        audioSource.Stop();
        //audioSource.volume = startVolume;
    }

    public static IEnumerator FadeIn(AudioSource audioSource, float FadeTime)
    {
        float startVolume = audioSource.volume;
        audioSource.Play();
        while (audioSource.volume < 1)
        {
            audioSource.volume += 1 * Time.deltaTime / FadeTime;

            yield return null;
        }

        //audioSource.Stop();
        //audioSource.volume = startVolume;
    }
}
