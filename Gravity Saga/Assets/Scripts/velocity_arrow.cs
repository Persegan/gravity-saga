﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class velocity_arrow : MonoBehaviour {
    public GameObject player;
    public GameObject arrow_body;

	// Use this for initialization
	void Start () {
        Vector3 pz = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        pz.z = 0;

        Vector3 player_mouse = pz - player.transform.position;

        gameObject.transform.position = player.transform.position + player_mouse.normalized * player.GetComponent<Player_Controller_Script>().auxiliar_velocity_magnitude * 0.1f;
        Vector3 finalvector = player.transform.position + player_mouse.normalized;

        float angle = Mathf.Atan2(player_mouse.y, player_mouse.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));

        Vector3[] line_positions = new Vector3[2];
        line_positions[0] = player.transform.position;
        line_positions[1] = gameObject.transform.position;
        arrow_body.GetComponent<LineRenderer>().SetPositions(line_positions);
    }
	
	// Update is called once per frame
	void Update () {
 
        Vector3 pz = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        pz.z = 0;

        Vector3 player_mouse = pz - player.transform.position;

        gameObject.transform.position = player.transform.position + player_mouse.normalized * player.GetComponent<Player_Controller_Script>().auxiliar_velocity_magnitude * 0.1f;
        Vector3 finalvector = player.transform.position + player_mouse.normalized;

       float  angle = Mathf.Atan2(player_mouse.y, player_mouse.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));

        Vector3[] line_positions = new Vector3[2];
        line_positions[0] = player.transform.position;
        line_positions[0].z = -9;
        line_positions[1] = gameObject.transform.position;
        line_positions[1].z = -9;
        arrow_body.GetComponent<LineRenderer>().SetPositions(line_positions);


    }
}
