﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SawBlade_behaviour : MonoBehaviour {

    public float angular_velocity;
    public Vector3[] movement_goals;
    private int movement_state = 0;
    public float speed;

	// Use this for initialization
	void Start () {
        if (GetComponent<Rigidbody2D>())
        GetComponent<Rigidbody2D>().angularVelocity = angular_velocity;
	}
	
	// Update is called once per frame
	void Update () {
        
         /* if (Player_Controller_Script.using_Sylph == false)
        {
            if (GetComponent<Rigidbody2D>())
                GetComponent<Rigidbody2D>().angularVelocity = angular_velocity;

            if (transform.position != movement_goals[movement_state])
            {
                transform.position = Vector3.MoveTowards(transform.position, movement_goals[movement_state], speed * Time.deltaTime);
            }
            else
            movement_state++;
            if (movement_state == movement_goals.Length)
            {
                movement_state = 0;
            }
        }
        
        else
        { 
            if (GetComponent<Rigidbody2D>())
            GetComponent<Rigidbody2D>().angularVelocity = 0;
        }   */    
          
          

    }
    
    void FixedUpdate()
    {
        if (Player_Controller_Script.using_Sylph == false)
        {
            if (GetComponent<Rigidbody2D>())
                GetComponent<Rigidbody2D>().angularVelocity = angular_velocity;

            if ((movement_goals[movement_state] - transform.position).magnitude > 0.1f)
            {
                //transform.position = Vector3.MoveTowards(transform.position, movement_goals[movement_state], speed * Time.deltaTime);
                GetComponent<Rigidbody2D>().velocity = movement_goals[movement_state] - transform.position;
                GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity.normalized * speed * Time.deltaTime;
            }
            else
                movement_state++;
            if (movement_state == movement_goals.Length)
            {
                movement_state = 0;
            }
        }

        else
        {
            if (GetComponent<Rigidbody2D>())
                GetComponent<Rigidbody2D>().angularVelocity = 0;
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        }
    }
}
