﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camera_script : MonoBehaviour {

    public float horizontaloffset;
    public float verticaloffset;

    public float speed;

    GameObject player;

	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        Application.targetFrameRate = 60;
	}
	
	// Update is called once per frame
	void LateUpdate () {
        //transform.position = new Vector3(player.transform.position.x+horizontaloffset, player.transform.position.y + verticaloffset, transform.position.z);
        Vector3 auxiliar = player.transform.position;
        auxiliar.x += horizontaloffset;
        auxiliar.y += verticaloffset;
        auxiliar.z = transform.position.z;
        transform.position = Vector3.Lerp(transform.position, auxiliar, speed * Time.deltaTime);
    }
}
