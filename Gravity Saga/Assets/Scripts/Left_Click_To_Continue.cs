﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Left_Click_To_Continue : MonoBehaviour {
    public string level_to_load;
    public Image black_background;

    // Use this for initialization
    void Start () {
        AudioListener.volume = 0.5f;
        black_background.gameObject.SetActive(true);
        black_background.canvasRenderer.SetAlpha(1.0f);
        black_background.CrossFadeAlpha(0.0f, 2.0f, false);

    }
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Fire1"))
        {
            StartCoroutine(Load_Next_Level());
        }
	}

    IEnumerator Load_Next_Level()
    {
        black_background.canvasRenderer.SetAlpha(0.0f);
        black_background.CrossFadeAlpha(1.0f, 2.0f, false);
        yield return new WaitForSeconds(2.0f);
        SceneManager.LoadScene(level_to_load);        
    }

}
