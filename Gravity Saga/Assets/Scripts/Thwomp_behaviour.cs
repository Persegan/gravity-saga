﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thwomp_behaviour : MonoBehaviour {

    public GameObject Thwomp;
    public float falling_speed;
    public float recovery_speed;

    public Transform groundCheck;
    float groundRadius = 0.025f;
    public LayerMask whatIsGround;

    private bool Thwomp_Falling = false;

    private Vector3 original_thwomp_position;

	// Use this for initialization
	void Start () {
        Thwomp_Falling = false;
        original_thwomp_position = Thwomp.transform.position;
	}
	
	// Update is called once per frame
	void Update () {

    }


    void OnTriggerStay2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            if (Thwomp_Falling == false)
            {
                StartCoroutine(Activate_Thwomp());
            }
        }
    }


    IEnumerator Activate_Thwomp()
    {
        Thwomp_Falling = true;

        while (Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround) == false)
        {
            if (Player_Controller_Script.using_Sylph == false)
            Thwomp.transform.position += new Vector3(0,-1,0) * falling_speed;
            yield return new WaitForEndOfFrame();
        }
        float timetowait = 1.0f;
        while (timetowait > 0.0f)
        {
            if (Player_Controller_Script.using_Sylph == false)
                timetowait -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        //yield return new WaitForSeconds(1.0f);
        BoxCollider2D[] Thwomp_Colliders = Thwomp.GetComponents<BoxCollider2D>();
        for (int i = 0; i < Thwomp_Colliders.Length; i++)
        {
            if (Thwomp_Colliders[i].isTrigger == true)
            {
                Thwomp_Colliders[i].enabled = false;
            }
        }
        while (Thwomp.transform.position != original_thwomp_position)
        {
            if (Player_Controller_Script.using_Sylph == false)
            Thwomp.transform.position = Vector3.MoveTowards(Thwomp.transform.position, original_thwomp_position, recovery_speed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }

        for (int i = 0; i < Thwomp_Colliders.Length; i++)
        {
            if (Thwomp_Colliders[i].isTrigger == true)
            {
                Thwomp_Colliders[i].enabled = true;
            }
        }

        Thwomp_Falling = false;
    }
}
