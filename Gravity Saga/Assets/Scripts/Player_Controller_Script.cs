﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player_Controller_Script : MonoBehaviour {

    public AudioClip jump;
    public AudioClip land;
    public AudioClip step;
    public AudioClip dash;
    public AudioClip die;
    public Image black_background;
    public ParticleSystem charging_particles;
    public ParticleSystem moving_particles;
    public GameObject arrow_head;
    public GameObject arrow_body;
    public float maxSpeed = 20f;
    public float acceleration;
    bool facingRight = true;
    public float horizontaldrag;
    Animator anim;
    new Rigidbody2D rigidbody;

    bool airborne = false;
    public Transform groundCheck;
    float groundRadius = 0.3f;
    public LayerMask whatIsGround;
    public float initialjumpForce = 70000;
    public float addedjumpForce;
    public float jumpForcedecreaserate;
    float auxiliarjumpForce;
    bool canjumphigher = true;
    bool canmove = true;
    bool wasairborne;
    public float velocity_maxSpeed;
    public float velocity_acceleration;
    public float velocity_horizontaldrag;
    public Vector3 spawn_position;

    public Slider Sylph_Slider;
    public float Sylph_Cooldown;

    public static bool using_Sylph
    {
        get;
        set;
    }

    public GameObject next_level
    {
        get;
        set;
    }

    public float auxiliar_velocity_magnitude
    {
        get;
        set;
    }



    public bool can_transition
    {
        get;
        set;
    }

    float auxiliar_gravity_scale;
    float auxiliar_acceleration;
    float auxiliar_horizontaldrag;
    float auxiliar_maxspeed;

    bool particlesmoving;

    private bool can_use_Sylph;
    private GameObject moving_platform;
    private Vector2 oldplatformvelocity = Vector2.zero;

    AudioSource BGM;


    // Use this for initialization
    void Start () {
        using_Sylph = false;
        can_use_Sylph = true;
        Sylph_Slider.maxValue = Sylph_Cooldown;
        Sylph_Slider.gameObject.SetActive(false);
        anim = GetComponent<Animator>();
        rigidbody = GetComponent<Rigidbody2D>();
        arrow_body.GetComponent<LineRenderer>().enabled = false;
        arrow_head.GetComponent<SpriteRenderer>().enabled = false;
        BGM = GameObject.FindGameObjectWithTag("BGM").GetComponent<AudioSource>();
        transform.position = spawn_position;
        auxiliar_gravity_scale = rigidbody.gravityScale;
        auxiliar_maxspeed = maxSpeed;
        auxiliar_acceleration = acceleration;
        auxiliar_horizontaldrag = horizontaldrag;
    }
	

	void FixedUpdate ()
    {

        Collider2D col = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);
        airborne = !Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);
        if (col != null && col.gameObject.tag == "Moving_Platform")
        {
            bool already_zeroed = false;
            moving_platform = col.gameObject;
            if (oldplatformvelocity == Vector2.zero)
            {
                oldplatformvelocity.x = moving_platform.GetComponent<Rigidbody2D>().velocity.x;
                oldplatformvelocity.y = moving_platform.GetComponent<Rigidbody2D>().velocity.y;
            }
            if ((oldplatformvelocity.x < 0 && moving_platform.GetComponent<Rigidbody2D>().velocity.x > 0) || oldplatformvelocity.x > 0 && moving_platform.GetComponent<Rigidbody2D>().velocity.x < 0)
            {
                //rigidbody.velocity += col.gameObject.GetComponent<Rigidbody2D>().velocity * 2.75f;
                if (already_zeroed == false)
                {
                    rigidbody.velocity = Vector2.zero;
                    already_zeroed = true;
                }
                Vector2 auxiliar_vector2 = new Vector2(col.gameObject.GetComponent<Rigidbody2D>().velocity.x * 2, 0);

                rigidbody.velocity += auxiliar_vector2;
            }
            else
            {
                Vector2 auxiliar_vector2 = new Vector2(col.gameObject.GetComponent<Rigidbody2D>().velocity.x /4, 0);
                rigidbody.velocity += auxiliar_vector2;
            }
            if ((oldplatformvelocity.y < 0 && moving_platform.GetComponent<Rigidbody2D>().velocity.y > 0) || oldplatformvelocity.y > 0 && moving_platform.GetComponent<Rigidbody2D>().velocity.y < 0)
            {
                //rigidbody.velocity += col.gameObject.GetComponent<Rigidbody2D>().velocity * 2.75f;
                if (already_zeroed == false)
                {
                    rigidbody.velocity = Vector2.zero;
                    already_zeroed = true;
                }

                Vector2 auxiliar_vector2 = new Vector2(0, col.gameObject.GetComponent<Rigidbody2D>().velocity.y*1.5f);

                rigidbody.velocity += auxiliar_vector2;
            }
            else
            {
                Vector2 auxiliar_vector2 = new Vector2(0, col.gameObject.GetComponent<Rigidbody2D>().velocity.y/10);
                rigidbody.velocity += auxiliar_vector2;
            }
            oldplatformvelocity.x = moving_platform.GetComponent<Rigidbody2D>().velocity.x;
            oldplatformvelocity.y = moving_platform.GetComponent<Rigidbody2D>().velocity.y;
        }
        else
        {
            moving_platform = null;
            oldplatformvelocity = Vector2.zero;
            //transform.parent = null;
        }
        anim.SetBool("airborne", airborne);
        anim.SetFloat("vertical_speed", GetComponent<Rigidbody2D>().velocity.y);


        if (moving_platform == null)
        {
            anim.SetFloat("speed", Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x));
            if (GetComponent<Rigidbody2D>().velocity.x > 0.5f && !facingRight && Input.GetAxis("Horizontal") != 0)
                Flip();

            else if (GetComponent<Rigidbody2D>().velocity.x < -0.5 && facingRight && Input.GetAxis("Horizontal") != 0)
                Flip();
        }
        else
        {
            float auxiliar;
            auxiliar = GetComponent<Rigidbody2D>().velocity.x - moving_platform.GetComponent<Rigidbody2D>().velocity.x * 1.25f;
            if (Input.GetAxis("Horizontal") == 0)
                auxiliar = 0;
            anim.SetFloat("speed", Mathf.Abs(auxiliar));

            if ( auxiliar > 0.5f && !facingRight)
                Flip();

            else if (auxiliar < -0.5 && facingRight)
                Flip();
        }



        //Speed limiter
        if (Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x) > maxSpeed)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(rigidbody.velocity.normalized.x * maxSpeed, rigidbody.velocity.y);
        }

        //Horizontal drag
        /*if (!airborne && Input.GetAxisRaw("Horizontal") == 0)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(rigidbody.velocity.x * horizontaldrag, rigidbody.velocity.y);
        }*/
        GetComponent<Rigidbody2D>().velocity = new Vector2(rigidbody.velocity.x * horizontaldrag, rigidbody.velocity.y);
    }

    // Update is called once per frame
    void Update()
    {
            if (wasairborne == true && airborne == false)
            {
                GetComponent<AudioSource>().PlayOneShot(land);
            }
            if (particlesmoving == true)
            {
                if (!moving_particles.isPlaying)
                {
                    moving_particles.Play();
                }
                if (!airborne)
                {
                    moving_particles.Stop();
                    particlesmoving = false;
                }
            }

           

            if (Input.GetAxisRaw("Horizontal") == 1)
            {
                if (canmove == true)
              {
                GetComponent<Rigidbody2D>().AddForce(new Vector2(acceleration, 0));
              }

                  
            }
            else if (Input.GetAxisRaw("Horizontal") == -1)
            {
                if (canmove == true)
                    GetComponent<Rigidbody2D>().AddForce(new Vector2(-acceleration, 0));
            }
            if (can_use_Sylph == true)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                if (canmove == true)
                {
                    using_Sylph = true;
                    BGM.volume = 0.25f;
                    black_background.gameObject.SetActive(true);
                    black_background.GetComponent<CanvasRenderer>().SetAlpha(0.0f);
                    black_background.CrossFadeAlpha(128 / 255f, 0.2f, false);
                    moving_particles.Stop();
                    particlesmoving = false;
                    charging_particles.gameObject.SetActive(true);
                    charging_particles.Play();
                    arrow_body.GetComponent<LineRenderer>().enabled = true;
                    arrow_head.GetComponent<SpriteRenderer>().enabled = true;
                    canmove = false;
                    auxiliar_velocity_magnitude = rigidbody.velocity.magnitude;
                    anim.enabled = false;
                    rigidbody.gravityScale = 0;
                    rigidbody.velocity = new Vector2(0, 0);
                }

            }
            else if (Input.GetButton("Fire1"))
            {
                if (canmove == false)
                {
                    using_Sylph = true;
                    rigidbody.gravityScale = 0;
                    rigidbody.velocity = new Vector2(0, 0);
                }
              
            }
            else if (Input.GetButtonUp("Fire1"))
            {
                if (rigidbody.gravityScale == 0)
                {
                    using_Sylph = false;
                    GetComponent<AudioSource>().PlayOneShot(dash);
                    BGM.volume = 1.0f;
                    black_background.gameObject.SetActive(false);
                    charging_particles.gameObject.SetActive(false);
                    moving_particles.Play();
                    particlesmoving = true;
                    arrow_body.GetComponent<LineRenderer>().enabled = false;
                    arrow_head.GetComponent<SpriteRenderer>().enabled = false;
                    StopAllCoroutines();
                    StartCoroutine(velocity_boost());
                    can_use_Sylph = false;
                    StartCoroutine(Sylph_Cooldown_Coroutine());
                }
                   
               
            }
        }
           

        if (Input.GetAxisRaw("Vertical") == 1)
        {
            if (can_transition == true)
            {
                next_level.GetComponent<Level_Transition>().final_transition();
                gameObject.SetActive(false);
            }
        }

        if (canmove == true)
        {
            if (!airborne && Input.GetButtonDown("Jump") == true)
            {
                canjumphigher = true;
                auxiliarjumpForce = addedjumpForce;
                //anim.SetBool("airborne", true);
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0, initialjumpForce));
                GetComponent<AudioSource>().PlayOneShot(jump);
            }

            if (Input.GetButtonUp("Jump"))
            {
                canjumphigher = false;
            }
            if (Input.GetButton("Jump") && rigidbody.velocity.y > 0 && canjumphigher == true)
            {
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0, auxiliarjumpForce));
                auxiliarjumpForce = auxiliarjumpForce * jumpForcedecreaserate;
            }
        }
        wasairborne = airborne;

        Vector3 auxiliar = gameObject.transform.position;
        auxiliar.y += 1.5f;
        Sylph_Slider.gameObject.transform.position = auxiliar;
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Next_Level")
        {
            can_transition = true;
            next_level = collider.gameObject;
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.tag == "Next_Level")
        {
            can_transition = false;
        }
    }

    void OnTriggerStay2D(Collider2D collider)
    {
        if (collider.tag == "deadly")
        {
            Die();
        }
    }

    void Flip()
    {
        facingRight = !facingRight;
        transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }

    IEnumerator velocity_boost()
    {
        //auxiliar_horizontaldrag = horizontaldrag;
        //auxiliar_acceleration = acceleration;
        //auxiliar_maxspeed = maxSpeed;

        horizontaldrag = velocity_horizontaldrag;
        //maxSpeed = velocity_maxSpeed;
        acceleration = velocity_acceleration;


        anim.enabled = true;
        Vector3 pz = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        pz.z = 0;
        Vector3 newvelocity = pz - gameObject.transform.position;
        rigidbody.velocity = newvelocity;
        if (GetComponent<Rigidbody2D>().velocity.x > 0.5f && !facingRight)
            Flip();

        else if (GetComponent<Rigidbody2D>().velocity.x < -0.5 && facingRight)
            Flip();

        rigidbody.velocity = rigidbody.velocity.normalized * auxiliar_velocity_magnitude;
        maxSpeed = Mathf.Abs(rigidbody.velocity.x);
        float lastmaxSpeed = maxSpeed;
        rigidbody.gravityScale = auxiliar_gravity_scale;
        canmove = true;

        
        while (airborne)
        {
            if (Mathf.Abs(rigidbody.velocity.x) < lastmaxSpeed)
                maxSpeed = Mathf.Abs(rigidbody.velocity.x);
            if (maxSpeed < auxiliar_maxspeed)
                maxSpeed = auxiliar_maxspeed;

            lastmaxSpeed = maxSpeed;
            yield return new WaitForEndOfFrame();
        }

        horizontaldrag = auxiliar_horizontaldrag;
        acceleration = auxiliar_acceleration;
        maxSpeed = auxiliar_maxspeed;

    }

    public void StepSound()
    {
        GetComponent<AudioSource>().PlayOneShot(step);
    }

    public void Die()
    {
        rigidbody.velocity = Vector2.zero;
        transform.position = spawn_position;
        GetComponent<AudioSource>().PlayOneShot(die);
        StopCoroutine(Sylph_Cooldown_Coroutine());
        Sylph_Slider.gameObject.SetActive(false);
        can_use_Sylph = true;
    }


    IEnumerator Sylph_Cooldown_Coroutine()
    {
        Sylph_Slider.gameObject.SetActive(true);
        Sylph_Slider.value = 0;

        for (float i = 0; i < Sylph_Cooldown; i +=0.05f)
        {
            Sylph_Slider.value += 0.05f;
            yield return new WaitForSeconds(0.05f);

        }
        Sylph_Slider.gameObject.SetActive(false);
        can_use_Sylph = true;
    }
}
