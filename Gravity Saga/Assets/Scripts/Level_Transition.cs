﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Level_Transition : MonoBehaviour {

    public Image black_background;
    public string next_level;

	// Use this for initialization
	void Start () {
        black_background.gameObject.SetActive(true);
        initial_transition();
	}
	
	// Update is called once per frame
	void Update () {


	
	}

    public void initial_transition()
    {
        StartCoroutine(initial_transition_Coroutine());
    }

    public void final_transition()
    {
        StartCoroutine(final_transition_Coroutine());
    }



    IEnumerator initial_transition_Coroutine()
    {
        black_background.gameObject.SetActive(true);
        Color temp = black_background.color;
        temp.a = 255;
        black_background.color = temp;
        black_background.CrossFadeAlpha(0, 0.5f, false);
        yield return new WaitForSeconds(0.5f);
    }

    IEnumerator final_transition_Coroutine()
    {
        black_background.gameObject.SetActive(true);
        black_background.canvasRenderer.SetAlpha(0.0f);
        black_background.CrossFadeAlpha(1.0f, 0.5f, false);
        SceneManager.LoadScene(next_level);
        yield return 0;
    }
}
