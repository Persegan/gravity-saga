﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SawBlade_behaviour_2 : MonoBehaviour {


    public float angular_velocity;
    public Vector3[] movement_goals;
    private int movement_state = 0;
    public float speed;
    public float waittime;

    private float currentwaittime;

    // Use this for initialization
    void Start()
    {
        currentwaittime = waittime;
        if (GetComponent<Rigidbody2D>())
            GetComponent<Rigidbody2D>().angularVelocity = angular_velocity;
    }


    void FixedUpdate()
    {
        if (Player_Controller_Script.using_Sylph == false)
        {
            if (GetComponent<Rigidbody2D>())
                GetComponent<Rigidbody2D>().angularVelocity = angular_velocity;

            if ((movement_goals[movement_state] - transform.position).magnitude > 0.1f)
            {
                GetComponent<Rigidbody2D>().velocity = movement_goals[movement_state] - transform.position;
                GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity.normalized * speed * Time.deltaTime;
            }
            else
            {
                GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                currentwaittime -= Time.deltaTime;
                if (currentwaittime <= 0.0f)
                {
                    movement_state++;
                    currentwaittime = waittime;
                }
            }
            if (movement_state == movement_goals.Length)
            {
                movement_state = 0;
            }
        }

        else
        {
            if (GetComponent<Rigidbody2D>())
                GetComponent<Rigidbody2D>().angularVelocity = 0;
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        }
    }
}
